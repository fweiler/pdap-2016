'''
TASKS
1. Read the NO2 data from one of the *.VisNO2A files.
2. Read the wind data from the file Meteo_HOURLY_DATA_OCT_2012_DEC_2013.csv and create a plot of wind speed vs. time for the same day which you chose in the previous step.
   ==> Convert to DD/MM/YYYY - select corresponding date
3. Create a figure with two plots: one plot of all NO2 measurements for the whole day (NO2 vs. time) and a histogram of all NO2 measurements.
4. Create a panel (figure) with one plot for each elevation angle (NO2 vs. time).
   ==> plo
5. Determine main statistics (min, max, mean, standard deviation) for the NO2 slant columns for each elevation angle.
'''

import glob
import numpy as np
import matplotlib.pyplot as plot
import datetime


    
def sliceByElevation(data, angle):
    '''
    func to extract from array a subarray which contains all measurements at a 
    desired elevation angle
    pass: dataframe containing all measurements, desired elevation angle
    returns: subarray with all data for given elevation angle
    '''
    if not data.shape[0] % 9 == 0: 
        print("Data doesn't fit scheme, cutting off first line, retrying")
        reduced_data = data[1:,:]
        sliceByElevation(reduced_data, angle)
    # Goal: Slice off first line of data and recursively call sliceByElevation
    NUM_OF_COL = int(data.shape[0]/9)
    NUM_OF_LINES = int(data.shape[1])
    
    # Create empty np.array with same number of colums but 1/9 of parent array lines
    sliced_data = np.empty([NUM_OF_COL, NUM_OF_LINES])
    
    j = 0 # Iterator
    for i in range(data.shape[0]):
        if data[i,6] == angle:              # Data in parent array matches
            sliced_data[j,:] = data[i,:]    # Data is written to subarray
            j = j + 1                       # Next line in subarray
                
    return sliced_data
    
def NO2_statistics(data): 
    no2values = data[:,13]
    minimum = np.min(no2values)
    maximum = np.max(no2values)
    mean = np.mean(no2values)
    return mean, minimum, maximum

# Task 1: Read the NO2 data from one of the *.VisNO2A files.
testfile = '../../data/no2-athens/130624SS.VisNO2A'    
data = np.genfromtxt(testfile, delimiter=None, comments = '*')
    
# Task 2: Read the wind data from the file 
# Meteo_HOURLY_DATA_OCT_2012_DEC_2013.csv 
# and create a plot of wind speed vs. time 
# for the same day which you chose in the previous step.

wind_data = np.genfromtxt(
fname = '../../data/no2-athens/Meteo_HOURLY_DATA_OCT_2012_DEC_2013.csv', 
delimiter = ',', 
skip_header = 1)

day_of_year = datetime.date(2013, 6, 24).timetuple().tm_yday

wind_speed_day = wind_data[wind_data[:,1] == day_of_year]

figWSpeed = plot.figure(figsize=(5.0,2.0))
axesWS = figWSpeed.add_subplot(1,1,1)
axesWS.set_xlabel('time') # needs unit
axesWS.set_ylabel('wind speed') # needs unit
axesWS.plot(wind_speed_day[:,5])

# Task 3: Create a figure with two plots: 
# one plot of all NO2 measurements for the whole day (NO2 vs. time) 
# and a histogram of all NO2 measurements.

# Create Figure with 2 Subfigures
task4 = plot.figure(figsize=(10.0, 5.0))
task4ax1 = task4.add_subplot(1, 2, 1)
task4ax1.set_ylabel('[n/cm^3]')
task4ax1.set_xlabel('Time of day')
task4ax2 = task4.add_subplot(1, 2, 2)
task4ax2.set_ylabel('rel Occurence') # not quite that relative yet, normed=True doesnt work?
task4ax2.set_xlabel('[n/cm^3]')

# Empty container for all no2values, needed for Histogram
histogram = []

# Keys for figure, not implemented yet
keys = ['SS', 'TS', 'VS', 'US', 'WS']

# Read all data files from 24th of June 2013
filesof24th = glob.glob('../../data/no2-athens/130624*.VisNO2A')

# Create No2 over time, key needed
for file in filesof24th:
    data24 = np.genfromtxt(file, delimiter=None, comments = '*')
    no2values = data24[:,13]
    times = data24[:,1]
    task4ax1.plot(times, no2values)
    histogram.append(no2values)

# Create stacked histogram, key needed
task4ax2.hist(histogram, range = (0.0, 3.0e17), bins = 20, normed=True, histtype='barstacked')

# Task 4: Create a panel (figure) with one plot 
# for each elevation angle (NO2 vs. time).

elevationangles = [90, 91, 92, 93, 94, 96, 98, 105, 120]
# List comprehension to generate list of arrays sliced by elevation angle
slicedbyElevation_container = [sliceByElevation(data, angle) for angle in elevationangles]
                               
figElev = plot.figure(figsize=(10.0, 3.0))

for i in range(9):
    axes = figElev.add_subplot(3, 3, i+1)
    axes.plot(slicedbyElevation_container[i][:,13])    
    
# Task 5: Determine main statistics (min, max, mean, standard deviation) 
# for the NO2 slant columns for each elevation angle.

means = []
minima = []
maxima = []
stdevs = []

for array in slicedbyElevation_container:
    means.append(np.mean(array[:,13]))
    minima.append(np.min(array[:,13]))
    maxima.append(np.max(array[:,13]))
    stdevs.append(np.std(array[:,13]))
    
for i in range(9):
    print('+++++++++++++++++')
    print('Elevation angle ' + str(elevationangles[i]) + '\n')
    print('Mean = ' + str(means[i]) + '\n')
    print('Max = ' + str(maxima[i]) + '\n')
    print('Min = ' + str(minima[i]) + '\n')
    print('Stddev = ' + str(stdevs[i]) + '\n')
    

print('               \_(ツ)_/¯' + '\n' + '            it\'s something')
    
