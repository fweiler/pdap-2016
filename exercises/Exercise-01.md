Title: Exercise 01
Date: 24 November 2016


This is the first homework exercise for the course *Practical Data Analysis with
Python* by Andreas Hilboll, given at the University of Bremen in the winter term
2016/17.


# Deadline

This optional exercise is due Wednesday, 30 November 2016, 10:00 CET.


# Overview

In this exercise you will analyze measurements of atmospheric nitrogen dioxide
(NO2) from a measurement station located in Athens, Greece.  The data are being
measured with the MAX-DOAS method.


# Background

NO2 is a trace gas which is produced mainly from the burning of fossil fuels;
other (natural) sources include biomass burning (forest fires, agricultural
fires), lightning, and microbial emissions from soils.

MAX-DOAS stands for *Multi-AXis Differential Optical Absorption Spectroscopy*.
The instrument consists of a telescope and a spectrometer, which measure the
intensity of scattered sunlight in different elevation and azimuth directions.
DOAS is an application of the Beer-Lambert law, in which the integrated trace
gas concentration along the average light path (from the sun to the instrument),
called *slant column* or *slant column density*, is derived from the trace gas'
absorption cross section (measured in the laboratory) and the attenuation of the
scattered sunlight in the atmosphere.  The slant column density is in units of
*molecules per ground area*.  As it depends strongly on the length of the light
path, it is larger close to sunrise and sunset, when the sun is low, compared to
midday, when the sun is high.

The elevation angle is the angle between the vertical (pointing downwards) and
the viewing elevation of the telescope, i.e., it is 90° for looking towards the
horizon and 180° for looking towards the zenith.

The azimuth angle is the geographical direction of the telescope line-of-sight.
In these data files, it is defined to go from -180° to 180°, with -90° being
East, 0° being South, and 90° being West.


# Objective

This exercise will teach you how to read measurement data from an ASCII file, to
calculate some basic statistics, and how to visualize the results.


# Tasks

1. Read the NO2 data from one of the `*.VisNO2A` files.
2. Read the wind data from the file `Meteo_HOURLY_DATA_OCT_2012_DEC_2013.csv`
   and create a plot of wind speed vs. time for the same day which you chose in
   the previous step.
3. Create a figure with two plots: one plot of all NO2 measurements for the
   whole day (NO2 vs. time) and a histogram of all NO2 measurements.
5. Create a panel (figure) with one plot for each elevation angle (NO2
   vs. time).
6. Determine main statistics (min, max, mean, standard deviation) for the NO2
   slant columns for each elevation angle.


# Hints

- All data files are located in the folder `data/no2-athens/`.
- The filename of the NO2 data files (`*.VisNO2A`) contains two pieces of
  information, the date (in the form `YYMMDD`) and one of five viewing azimuth
  directions (`SS`, `TS`, `US`, `VS`, `WS`).  For example, the file
  `130624VS.VisNO2A` contains measurements from 24 Jun 2013 for the azimuth
  direction `VS`.
- The NO2 slant column density is contained in the column *Schräge Säule NO2*
- The column *Day of Year 1993* contains the days which passed since
  1992-12-31T00:00:00 UTC.  This means that for example 23 Jun 2013 has values
  between 7479.0 and 7480.0.
- The column *Line of Sight* contains the elevation angle in degrees.
- Use the function `numpy.genfromtxt` for reading the meteorological data,
  **not** `numpy.loadtxt`.
- You can use the method `tight_layout()` to rearrange a Figure so that the
  labels don't overlap, e.g., `fig.tight_layout()` if `fig` is your Figure.
  
  
## General comments

- Use functions to make your code more understandable.
- Document your code, so that you still remember two weeks from now why you
  wrote the code you write.
- Use
  [defensive programming](https://swcarpentry.github.io/python-novice-inflammation/08-defensive/),
  to find mistakes early.


# Logistics

You can complete this exercise in teams of two or three students. Please let
`LASTNAMES` be a `+` separated list, for example
`LASTNAME1+LASTNAME2+LASTNAME3`.

1.  Fork the course repository https://gitlab.com/iup-bremen/pdap-2016 and check
    out a local working copy to your computer.
2.  Give at least *Reporter* permissions to my Gitlab user *andreas-h*.
3.  Add your name, study program, and computer details to the file
    `PARTICIPANTS.md`.
4.  Commit the changes to the *master* branch.
5.  Create a branch `ex01-LASTNAMES`.
6.  Copy this file (`exercises/Exercise-01.md`) to file
    `exercises/ex01/LASTNAMES.md`, and fill out the *Participants* section (see
    below).
7.  Create a file `exercises/ex01/LASTNAMES.py` and solve the tasks outlined
    above; commit that file to the `ex01-LASTNAMES` branch.
8.  If necessary, give any additional information I need in order to understand
    your code in the section *Solution* below.
9.  Feel free to give feedback to this exercise in the section *Feedback*
    below. Please indicate the total time you needed to complete this exercise.
10. Don't forget to commit and push your changes to Gitlab.


# Participants

Names of all members of this study group:

1. 
2. 
3. 


# Solution


# Feedback
