Title: Participant list

Please CAPITALIZE your last name, like this: *Firstname LASTNAME*.

| Name            | GitLab username | Study program | OS    | Architecture | Python version |
|-----------------|-----------------|---------------|-------|--------------|----------------|
| Andreas HILBOLL | andreas-h       | *Lecturer*    | Linux | 64bit        |            3.5 |
| Felix WEILER    | fweiler         | BMS: Bionik   | MacOSX| 64bit        |          3.5.2 |
| Corina VALTL    | corva           | BMS: Bionik   | MacOSX| 64bit        |          2.7.11|
| Leon DANTER	  | 0Nel            | BMS: Bionik   | MacOSX| 64bit        |         2.7.12 |
| Vincent FOCKE   | Chente          | BMS: Bionik   | MacOSX| 64bit        |         2.7.12 |
